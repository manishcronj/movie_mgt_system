



var path = require('path');
// var config = require('../config/environment');
var Sequelize = require('sequelize');
var lodash = require('lodash');


var sequelize = new Sequelize('postgres://postgres:cronj@localhost:5432/movie_management', {underscored:true,freezeTableName:true});

//totalModels are actually name of folders.Models are with '-' replaced with '_'
var totalModels = ['user'];

var db = {};

var modelPathFormat = path.join(__dirname, '../api/{0}/{1}.model.js');

for (var i in totalModels) {
  var modelFile = totalModels[i].replace(/-/g,'.');
  var model = require(modelPathFormat.replace(/\{0\}/g, totalModels[i]).replace(/\{1\}/g,modelFile))(sequelize,Sequelize);
  console.log("model: ",model.name);
  db[model.name] = model;
}

Object.keys(db).forEach(function(modelName) {
  if ('associate' in db[modelName]) {

    db[modelName].associate(db)
  }
})

module.exports = lodash.extend({
  sequelize: sequelize,
  Sequelize: Sequelize
}, db);
