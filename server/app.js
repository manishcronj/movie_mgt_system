var express = require('express');
var app = express();
var http = require('https');


var fs = require('fs');
var users = [];
var key = 0;
// require('./route.js')(app);

// var server=http.Server(app);
// var webrtc=require('webrtc.io');

var options = {
  // key: fs.readFileSync('server.key'),
  key: fs.readFileSync('certKey.pem'),
  cert:fs.readFileSync('Certificates.pem')
};

var server = require('./config/express.js')(options, app); 
var io = require('socket.io')(server);

io.on('connection',function(user){
	var tempId = user.id;
	console.log(user.id);
	var name = 'user'+ key;
	users.push({id:user.id,name:name});
	console.log(users);
	key++;
	console.log(key);
	io.emit('onlineusers',users);
    console.log('user connected');
	user.on('offer',function(data){
		console.log('datachutiyappa',data);
        user.broadcast.emit('offer',data.data);
	})

	user.on('answer',function(data){
		console.log('dataAnswer', data);
		user.broadcast.emit('answer',data.data);
	})
	user.on('message',function(data){
		user.broadcast.emit('message',data);
	})
	user.on('disconnect',function(){
		for(var i=0;i<users.length;i++){
			if(tempId == users[i].id){
				users.splice(i,1);
				break;
			}
		}

		key--;
		io.emit('onlineusers',users);
		console.log('user disconnected');
	})

	user.on('icecandidate',function(data){
		console.log('icecandidate',data);
		user.broadcast.emit('icecandidate',data);
	})
})
	

server.listen(3002,function(){
	console.log('server is listening on .....',3002);
    })



module.exports = app;
